from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram

def display_result(linkage_matrix):
    dendrogram(linkage_matrix, labels=[i+1 for i in range(len(linkage_matrix) + 1)])
    figure = plt.gcf()
    figure.canvas.set_window_title('Hierarchical graphic')
    plt.show()