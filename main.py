from sys import argv
import numpy as np
from hierarchical_clustering import get_linkage_matrix
from graphic import display_result

def main():
    size = int(argv[1])
    matrix = np.random.rand(size, size)
    matrix = (matrix + matrix.T)/2
    np.fill_diagonal(matrix, 0.)
    print(matrix)

    linkage_matrix = get_linkage_matrix(matrix)
    display_result(linkage_matrix)

main()
