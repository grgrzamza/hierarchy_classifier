from itertools import combinations
from collections import defaultdict

def pairs(iterable):
    return combinations(iterable, 2)

def distance(matrix, cluster1, cluster2):
    return min([matrix[x][y] for x in cluster1 for y in cluster2])

def get_linkage_matrix(distance_matrix):
    items_count = len(distance_matrix)
    result = []
    forest = [(i, [i]) for i in range(items_count)]
    for index in range(items_count - 1):
        vertex_pair = defaultdict(list)
        for first, second in pairs(forest):
            vertex_pair[distance(distance_matrix, first[1], second[1])] = (first, second)
        min_distance = min(vertex_pair.keys())
        clusters_pair = vertex_pair[min_distance]
        new_cluster_items = clusters_pair[0][1] + clusters_pair[1][1]
        new_forest = []
        for tree in forest:
            if tree not in clusters_pair:
                new_forest.append(tree)
        new_forest.append((items_count + index, new_cluster_items))
        forest = new_forest
        result.append([clusters_pair[0][0], clusters_pair[1][0], min_distance, len(new_cluster_items)])
    return result
